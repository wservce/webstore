<div class="row">
    <div class="col-md-5">
        <div class="row"><h2>Войти</h2></div>
        <div class="row">
            <form method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                           placeholder="Пароль">
                </div>
                <?php
                if($loginmsg)
                    echo "<div class='alert alert-danger'>$loginmsg</div>";
                ?>
                <button type="submit" class="btn btn-default" name="login">Войти</button>
            </form>
        </div>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-5">
        <div class="row"><h2>Зарегистрироваться</h2></div>
        <div class="row">
            <form method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                           placeholder="Пароль">
                </div>
                <?php
                if($regmsg)
                echo "<div class='alert alert-danger'>$regmsg</div>";
                ?>
                <button type="submit" class="btn btn-default" name="reg">Зарегистрироваться</button>
            </form>
        </div>
    </div>
</div>