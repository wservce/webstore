<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-md-3 logo-f">
                <img src="/img/webstore_w.png" alt="webstore_w">
                <div class="copy">
                    <p>WEBSTORE</p>
                    <p>все права защищены</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="foot-menu">
                    <ul>
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">Преимущества</a></li>
                        <li><a href="#">Демонстрация</a></li>
                        <li><a href="#">Услуги</a></li>
                        <li><a href="#">Наши клиенты</a></li>
                    </ul>
                </div>

            </div>
            <div class="col-xs-12 col-md-3">
                <div class="social">
                    <img src="/img/face.png" alt="face">
                    <img src="/img/vk.png" alt="webstore_w">
                    <img src="/img/insta.png" alt="webstore_w">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copy2">
                    Webservice Ukraine © 2017
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="/js/bootstrap.min.js"></script>
<script src="/js/inputmask.js"></script>
<script src="/js/snap.svg-min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/init.js"></script>
<script>
    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            margin: 60,
            items: 3,
            autoWidth: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5
                }
            }
        })
    });
</script>
</body>
</html>