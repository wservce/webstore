<?php
\system\Helpers::Block("HeaderController", "index");
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 alert alert-danger error404">
                <h1><?= $lang->getString("error_404"); ?></h1>
                <h3><?= $lang->getString("date_and_time"); ?> <?= date("d.m.Y H:i:s"); ?></h3>
                
            </div>

            <div class="clear"></div>
        </div>
    </div>
<?php
\system\Helpers::Block("FooterController", "index");
?>