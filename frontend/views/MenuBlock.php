<div class="hidden-xs hidden-sm menu-wrapper bold">
    <ul>
        <?php
        if (count($menu) > 0) {
            $i = 1;
            foreach ($menu as $l => $m) {
                ?>
                <li><a class="item<?= $i ?>" href="<?= $l ?>" <?php if ($l == $url) echo "class=\"act\""; ?>><?= $m ?></a><span class="point p<?= $i ?>"></span></li>
                <?
                $i++;
            }
        }
        ?>
    </ul>
</div>