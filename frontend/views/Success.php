<?php
\system\Helpers::Block("HeaderController", "index");
?>

    <div class="container">
        <div class="row">
            <h1><?=$lang->getString("success")?></h1>
            <div class="alert alert-success"><?=$lang->getString("success_msg")?></div>
        </div>
    </div>

<?
\system\Helpers::Block("FooterController", "index");
?>