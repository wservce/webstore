<?php
\system\Helpers::Block("HeaderController", "index");
?>
<?php
\system\Helpers::Block("MenuController", "index");
?>

    <main>
        <div class="container">
            <div class="row img-1-wrapper">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <img class="hidden-xs hidden-sm" src="/img/2.jpg" alt="2.jpg">
                    <img class="hidden-md hidden-lg" src="/img/macbook_header.png" alt="2.jpg">
                    <!--                <div class="text-uppercase slogan-1">
                                        <p>Хотите найти <span class="bold">решение?</span></p>
                                        <p>Ищите <span class="bold">здесь!</span></p>
                                    </div>
                                    <img class="hidden-md hidden-lg arrow-down" src="/img/arrow%20down.png"
                                         alt="arrow_down">-->
                </div>
                <div class="hidden-md hidden-lg col-xs-12 col-sm-12">
                    <div class="block-1-text">
                        <p><span>Web</span><span>Store</span></p>
                        <?= $text ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="block-svg">
                            <div class="road-wrapper">
                                <div class="img1"></div>
                                <div class="img2"></div>
                                <div class="img3"></div>
                            </div>
                            <svg id="svg_block_1"></svg>
                            <img class="hidden-xs img_1" src="/img/macbook-foodzilla-2.png"
                                 alt="macbook-foodzilla-2.png">
                            <div class="hidden-xs hidden-sm block-1-text">
                                <p><span>Web</span><span>Store</span></p>
                                <?= $text ?>
                            </div>
                            <svg id="svg_block_2"></svg>
                            <img class="hidden-xs hidden-sm img_2" src="/img/22.png" alt="macbook-foodzilla-2.png">
                            <div class="block-2-text">
                                <p>Универсальность</p>
                                <p>Разнообразный и богатый опыт рамки и место обучения кадров влечет за собой процесс
                                    внедрения
                                    и
                                    модернизации новых предложений. </p>
                            </div>
                            <svg id="svg_block_3"></svg>
                            <img class="hidden-xs hidden-sm img_3" src="/img/23.png" alt="hand.png">
                            <div class="block-3-text">
                                <p>Быстродействие</p>
                                <p>Задача организации, в особенности же реализация намеченных плановых заданий позволяет
                                    выполнять
                                    важные задания по разработке форм развития.</p>
                            </div>
                            <svg id="svg_block_4" class="hidden-xs">
                                <defs>
                                    <pattern id="img_4" width="1" height="1" x="0" y="0">
                                        <image xlink:href="/img/24.png" x="0" y="-1vw" width="39vw" height="50vw"/>
                                    </pattern>
                                </defs>
                            </svg>
                            <div class="block-4-text">
                                <p>Юзабельность</p>
                                <p>Задача организации, в особенности же реализация намеченных плановых заданий позволяет
                                    выполнять важные задания по разработке форм развития.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="content-back"></div>
            <div class="container">
                <div class="row demo-wrapper" id="demo-wrapper">
                    <div class="col-xs-12 col-md-3 text-center demo">
                        <p>Демонстрация</p>
                        <p>Задача организации, в особенности же реализация намеченных плановых заданий позволяет
                            выполнять
                            важные задания по разработке форм развития.</p>
                        <div class="hidden-xs line">
                            <a data-hover="Админка" class="mbtn mbtn-white"><span>Админка</span></a>
                            <a data-hover="Магазин" class="mbtn"><span>Магазин</span></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-9 demo-img">
                        <img src="/img/imac.png" alt="imac">
                        <img src="/img/macbook.png" alt="macbook">
                        <img class="hidden-xs" src="/img/ipad.png" alt="ipad">
                    </div>
                    <div class="hidden-md hidden-lg col-xs-12">
                        <div class="line">
                            <a data-hover="Админка" class="mbtn mbtn-white"><span>Админка</span></a>
                            <a data-hover="Магазин" class="mbtn"><span>Магазин</span></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="hidden-xs row">
                    <div class="col-md-12">
                        <div class="buy-wrapper" id="buy-wrapper">
                            <div class="buy-1">
                                <p>Для разработчиков</p>
                                <p>Сделай шаг в будущее с новой системой для интернет-магазина!</p>
                                <p><span>от</span> 500$</p>
                                <div class="topb"></div>
                                <ul>
                                    <li>Документация</li>
                                    <li>Легкость в разработке</li>
                                    <li>Готовые инструкции для менеджеров</li>
                                    <li>Быстродействие</li>
                                    <li>Неприхотливость</li>
                                </ul>
                                <a data-hover="Заказать" class="mbtn mbtn-black" href="/developers"><span>Заказать</span></a>
                            </div>
                            <div class="buy-2">
                                <p>Индивидуальный</p>
                                <p>Индивидуальный подход к решению поставленных задач оставит конкурентов позади.</p>
                                <p><span>от</span> 1000$</p>
                                <div class="topb"></div>
                                <ul>
                                    <li>+</li>
                                    <li>Индивидуальность</li>
                                    <li>Бесплатное размещение на сервере на 1 год</li>
                                    <li>Постоянная поддержка</li>
                                    <li>Быстрое выполнение</li>
                                </ul>
                                <a data-hover="Заказать" class="mbtn" href="/individual"><span>Заказать</span></a>
                            </div>
                            <div class="buy-3">
                                <p>Для бизнеса</p>
                                <p>Получите ощутимое преимущество для своего бизнеса в интернете!</p>
                                <p><span>от</span> 3000$</p>
                                <div class="topb"></div>
                                <ul>
                                    <li>+</li>
                                    <li>1 месяц SEO продвижения и контекстной рекламы</li>
                                    <li>Помощь во всех вопросах</li>
                                    <li>Мобильное приложение от компании <a href="https://mo-apps.com">MO APPS</a></li>
                                </ul>
                                <a data-hover="Заказать" class="mbtn mbtn-black" href="/mobile"><span>Заказать</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hidden-lg hidden-md price">
                    <div class="col-xs-12">
                        <h3 class="text-uppercase text-center title">Пакеты услуг</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title text-uppercase bold text-center">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Для разработчиков
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="buy-wrapper">
                                            <div class="buy-1">
                                                <p>Сделай шаг в будущее с новой системой для интернет-магазина!</p>
                                                <p><span>от</span> 500$</p>
                                                <div class="topb"></div>
                                                <ul>
                                                    <li>Документация</li>
                                                    <li>Легкость в разработке</li>
                                                    <li>Готовые инструкции для менеджеров</li>
                                                    <li>Быстродействие</li>
                                                    <li>Неприхотливость</li>
                                                </ul>
                                                <a data-hover="Заказать"
                                                   class="mbtn mbtn-black" href="/developers"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title  text-uppercase bold text-center">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Индивидуальный
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="buy-wrapper">
                                            <div class="buy-2">
                                                <p>Индивидуальный подход к решению поставленных задач оставит
                                                    конкурентов
                                                    позади.</p>
                                                <p><span>от</span> 1000$</p>
                                                <div class="topb"></div>
                                                <ul>
                                                    <li>+</li>
                                                    <li>Индивидуальность</li>
                                                    <li>Бесплатное размещение на сервере на 1 год</li>
                                                    <li>Постоянная поддержка</li>
                                                    <li>Быстрое выполнение</li>
                                                </ul>
                                                <a data-hover="Заказать" class="mbtn"href="/individual"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title  text-uppercase bold text-center">
                                        <a class="collapsed" role="button" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Для бизнеса
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="buy-wrapper">
                                            <div class="buy-3">
                                                <p>Получите ощутимое преимущество для своего бизнеса в интернете!</p>
                                                <p><span>от</span> 3000$</p>
                                                <div class="topb"></div>
                                                <ul>
                                                    <li>+</li>
                                                    <li>1 месяц SEO продвижения и контекстной рекламы</li>
                                                    <li>Помощь во всех вопросах</li>
                                                    <li>Мобильное приложение от компании <a href="https://mo-apps.com">MO
                                                            APPS</a></li>
                                                </ul>
                                                <a data-hover="Заказать"
                                                   class="mbtn mbtn-black"href="/mobile"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-12 works text-center" id="works">
                    <h2>СДЕЛАНО НА НАШЕЙ <span>CMS</span></h2>
                    <div class="owl-carousel owl-theme">
                        <div class="item"><img src="/img/dak.ws-at.com.png" alt=""></div>
                        <div class="item"><img src="/img/food.ws-at.com.png" alt=""></div>
                        <div class="item"><img src="/img/agrus.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="block-svg2">
                    <svg id="svg_block_5">
                        <defs>
                            <pattern id="img_5" width="100%" height="100%">
                                <image xlink:href="/img/keyboard.jpg" x="0" y="0" width="100%"/>
                            </pattern>
                        </defs>
                    </svg>
                    <div class="block-5-text">
                        <form class="form-inline" action="">
                            <div class="form-group">
                                <label for="mail">Возникли вопросы?</label>
                                <input id="mail" name="mail" class="form-control" type="text" placeholder="E-mail">
                                <div id="send"><img src="/img/send-dis.png" alt="send-dis"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="/js/svg.js"></script>
<?
\system\Helpers::Block("FooterController", "index");
?>