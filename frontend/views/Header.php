<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <meta name="keywords" content="<?= $key ?>"/>
    <meta name="description" content="<?= $desc ?>"/>
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <?php
    $css->includeCss('/css/bootstrap.min.css');
    $css->includeCss('/css/owl.carousel.css');
    $css->includeCss('/css/owl.theme.default.css');
    $css->includeCss('/css/style.css');
    $css->includeCss('/css/adaptive.css');
    $css->writeCss('/css/full.css');
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="msg-wrapper">
    <div class="msg-img">
        <img src="/img/horse-full.png" alt="horse-full">
    </div>
    <span class="close">+</span>
    <form>
        <h2 class="text-right bold text-uppercase">Ход за вами!</h2>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Телефон*">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="E-mail*">
        </div>
        <div class="form-group">
            <textarea name="" class="form-control" cols="20" rows="8" placeholder="Ваше сообщение"></textarea>
        </div>
        <div class="form-group">
            <a onclick="msgBoxShow();" data-hover="Отправить" class="mbtn mbtn-black"><span>Отправить</span></a>
        </div>
    </form>
</div>
<div class="overlay"></div>
<header>
    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 col-md-6">
                    <a class="logo-wrapper" href="/">
                        <img src="/img/webstore_w.png" alt='ws_logo_white'>
                    </a>
                </div>
                <div class="col-xs-8 col-md-6 order-btn text-right">
                    <a href="/profile" data-hover="Личный кабинет"
                       class="mbtn"><span>Личный кабинет</span></a>
                    <span class="callback"></span>
                    <a onclick="msgBoxShow();" data-hover="Оставить заявку"
                       class="mbtn"><span>Оставить заявку</span></a>
                    <span class="callback"></span>
                </div>
            </div>
        </div>
        <!--        <div class="menu-btn">
                    Меню
                </div>-->
    </div>
</header>