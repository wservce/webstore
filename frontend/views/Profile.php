<?php
\system\Helpers::Block("HeaderController", "index");
?>
    <div class="container">
        <div class="row">
            <h1><?= $title ?></h1>
        </div>
        <div class="row"></div>
        <?
        if (!isset($_SESSION['user'])) {
            \system\Helpers::Block("LoginController", "index");
        } else {
            if ($html) {
                echo "<div class='alert alert-success text-center'>".$html."</div>";
            } else {
                ?>
                <div class="col-md-6"><a href="#" data-hover="Скачать" class="mbtn"><span>Скачать</span></a></div>
                <div class="col-md-6"></div>
                <form method="post">
                <table class="table table-striped">
                    <tr>
                        <td>Домен 1</td>
                        <td>Ключ</td>
                        <td>Домен 2</td>
                        <td>Ключ</td>
                    </tr>
                    <?php 
                    if(count($orders)>0){
                        foreach($orders as &$o){
                            ?>
                            <?php
                            if($o['pay']==1) {
                                ?>
                                <tr>
                                    <td><?= $o['domain1'] ?></td>
                                    <td><?= $o['key1'] ?></td>
                                    <td><?= $o['domain2'] ?></td>
                                    <td><?= $o['key2'] ?></td>
                                </tr>
                                <?php
                            }else{
                                ?>
                                <tr>
                                    <td><?= $o['domain1'] ?></td>
                                    <td><?= $o['domain2'] ?></td>
                                    <td colspan="2" class="text-right"><button class="btn btn-success" type="submit" name="pay" value="<?=$o['id']?>">Оплатить</button></td>
                                </tr>
                                <?php
                            }
                            
                        }
                    }else{

                    }
                    ?>
                </table>
                </form>
                <?php
            }
        }
        ?>
    </div>
<?
\system\Helpers::Block("FooterController", "index");
?>