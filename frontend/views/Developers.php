<?php
\system\Helpers::Block("HeaderController", "index");
?>
    <div class="container">
        <div class="row">
            <h1><?= $page['name'] ?></h1>
            <?= $page['text'] ?>
        </div>
        <div class="row"></div>
        <?
        if (!isset($_SESSION['user'])) {
            \system\Helpers::Block("LoginController", "index");
        } else {
            if ($html) {
                echo "<div class='alert alert-success text-center'>".$html."</div>";
            } else {
                ?>
                <form method="post">
                    <div class="form-group">
                        <label for="domain1">Домен 1</label>
                        <input type="text" class="form-control" name="domain1" id="domain1" placeholder="Домен">
                    </div>
                    <div class="form-group">
                        <label for="domain2">Домен 2</label>
                        <input type="text" class="form-control" name="domain2" id="domain2"
                               placeholder="Пароль">
                    </div>
                    <?php
                    if ($msg)
                        echo "<div class='alert alert-danger'>$msg</div>";
                    ?>
                    <button type="submit" class="btn btn-success" name="order">Заказать</button>
                </form>
                <?php
            }
        }
        ?>
    </div>
<?
\system\Helpers::Block("FooterController", "index");
?>