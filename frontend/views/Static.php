<?php
\system\Helpers::Block("HeaderController", "index");
?>

    <div class="container">
        <div class="row">
            <h1><?=$page['name']?></h1>
            <?= $page['text'] ?>
        </div>
    </div>

<?
\system\Helpers::Block("FooterController", "index");
?>