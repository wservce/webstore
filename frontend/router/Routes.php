<?php

$routes = [
    'rules' =>
        [
            '/' => 'HomeController/index',
            '/404/' => 'Error404Controller/index',
            '/developers/' => 'OrderController/developers',
            '/individual/' => 'OrderController/individual',
            '/profile/' => 'LoginController/profile',
            '/mobile/' => 'OrderController/mobile',
            '/order/success/' => 'OrderController/success',
            '/order/confirm/' => "OrderController/confirm",
            '/<-ss>/' => "StaticController/index",
            '/<-controller>/<-action>/' => "<-controller>/<-action>",
        ],
];

return $routes;
