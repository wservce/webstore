<?php
namespace frontend\models;
use \system\App;
use \prototypes\Models;
use system\Helpers;

class OrderModel extends Models
{
    function getOrderHash($id)
    {
        $sql = "SELECT * FROM `orders` WHERE id = ?";
        $res = App::$app->modules->db->sql($sql, array("i", $id));
        return $this->toRow($res);
    }
    function getOrdersUser($id)
    {
        $sql = "SELECT * FROM `orders` WHERE id = ?";
        $res = App::$app->modules->db->sql($sql, array("i", $id));
        return $this->toArray($res);
    }
    function addOrder($domain1,$domain2,$key1,$key2,$hash,$user){
        $sql = "INSERT INTO `orders`(domain1,domain2,key1,key2,hash,`user`) VALUES (?,?,?,?,?,?)";
        App::$app->modules->db->sql($sql, array("sssssi", $domain1,$domain2,$key1,$key2,$hash,$user));
        return App::$app->modules->db->last_id();
    }
    function setOrderPay($id,$status){
        $sql = "UPDATE `orders` SET pay = ? WHERE `id` = ?";
        App::$app->modules->db->sql($sql, array("ii", $status,$id));
        return App::$app->modules->db->last_id();
    }
}

?>