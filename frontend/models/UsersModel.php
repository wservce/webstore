<?php
namespace frontend\models;
use \system\App;
use \prototypes\Models;
use system\Helpers;

class UsersModel extends Models
{

    function getOneUser($id)
    {
        $sql = "SELECT * FROM `users` WHERE id = ?";
        $res = App::$app->modules->db->sql($sql, array("i", $id));
        return $this->toRow($res);
    }
    function getOneUserByEmail($email)
    {
        $sql = "SELECT * FROM `users` WHERE email = ?";
        $res = App::$app->modules->db->sql($sql, array("s", $email));
        return $this->toRow($res);
    }
    function addUser($email,$pass){
        $sql = "INSERT INTO `users`(email,password) VALUES (?,?)";
        App::$app->modules->db->sql($sql, array("ss", $email,$pass));
        return App::$app->modules->db->last_id();
    }
}

?>