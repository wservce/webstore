<?php
namespace frontend\models;
use \system\App;
use \prototypes\Models;
use system\Helpers;

class LangModel extends Models
{
    function getLangs()
    {
        $sql = "SELECT `prefix` FROM `langs` WHERE `prefix` <> ?";
        $res = App::$app->modules->db->sql($sql, array("s", $_SESSION['lang']));
        return $this->toArray($res);
    }
    function getOneLang($lang)
    {
        $sql = "SELECT `id` FROM `langs` WHERE `prefix` = ?";
        $res = App::$app->modules->db->sql($sql, array("s", $lang));
        return $this->toRow($res); 
    }
}x

?>