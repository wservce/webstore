<?php
namespace frontend\models;
use \system\App;
use \prototypes\Models;
use system\Helpers;

class StaticModel extends Models
{
    
    function getOnePageByUrl($lang,$url)
    {
        $sql = "SELECT * FROM `static` c LEFT JOIN static_text t ON c.id = t.static_id WHERE t.lang_id = ? AND c.url = ?";
        $res = App::$app->modules->db->sql($sql, array("is", $lang,$url));
        return $this->toRow($res);
    }
}

?>