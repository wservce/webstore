<?php
namespace frontend\controllers;
use frontend\models\LangModel;
use prototypes\Controllers;
class LangController extends Controllers
{
    function __construct()
    {
        parent::__construct();
        $this->model = new LangModel();
    }
    function langPanel(){
        
        $this->data['langs'] = $this->model->getLangs();
        $this->data['url'] = str_replace($_SESSION['prefix'],'',$_SERVER['REQUEST_URI']);
        $this->renderBlock("LangPanel");
    }
}

?>