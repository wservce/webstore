<?php
namespace frontend\controllers;
use frontend\models\StaticModel;
use frontend\models\LangModel;
use prototypes\Controllers;
use system\App;
use system\Helpers;

class StaticController extends Controllers
{
    function __construct()
    {
        parent::__construct();
        $this->model = new StaticModel();
    }
    function index(){
        $url = array_keys(App::$app->PATH)[0];
        $langModel = new LangModel();
        $lang = $langModel->getOneLang($_SESSION['lang']);
        $this->data['page'] = $this->model->getOnePageByUrl($lang['id'],$url);
        if($this->data['page']['id']) {
            $this->data['title'] = $this->data['page']['title'];
            $this->data['desc'] = $this->data['page']['descriptions'];
            $this->data['key'] = $this->data['page']['keywords'];
            $this->render("Static");
        }else{
            header("Location: /404/");
        }
    }
    function block($id){
        $langModel = new LangModel();
        $lang = $langModel->getOneLang($_SESSION['lang']);
        $this->data['page'] = $this->model->getOnePageById($lang['id'],$id);
        $this->renderBlock("StaticBlock");
    }
}

?>