<?php
namespace frontend\controllers;

use frontend\models\OrderModel;
use frontend\models\StaticModel;
use frontend\models\LangModel;
use frontend\models\UsersModel;
use prototypes\Controllers;
use system\App;
use system\Helpers;
use system\modules\Validation;

class LoginController extends Controllers
{
    function __construct()
    {
        parent::__construct();
        $this->model = new UsersModel();
    }

    function index()
    {
        $validator = new Validation();
        if (isset($_POST['reg'])) {
            if ($validator->email($_POST['email'])) {
                $user = $this->model->getOneUserByEmail($_POST['email']);
                if (!$user['id']) {
                    if ($validator->password($_POST['password'])) {
                        $id = $this->model->addUser($_POST['email'], password_hash($_POST['password'], PASSWORD_DEFAULT));
                        $_SESSION['user'] = $id;
                    } else {
                        $this->data['regmsg'] = "Пароль должен быть длиннее 6 символов и содержать цифры и буквыразного регистра";
                    }
                } else {
                    $this->data['regmsg'] = "Email уже занят";
                }
            } else {
                $this->data['regmsg'] = "Не верный адресс почти";
            }
        } elseif (isset($_POST['login'])) {
            if ($validator->email($_POST['email'])) {
                $user = $this->model->getOneUserByEmail($_POST['email']);
                if ($user['id']) {
                    if (password_verify($_POST['password'], $user['password'])) {
                        $_SESSION['user'] = $user['id'];
                    } else {
                        $this->data['loginmsg'] = "Не верный пароль";
                    }
                } else {
                    $this->data['loginmsg'] = "Пользователь не найден";
                }
            } else {
                $this->data['loginmsg'] = "Заполните все поля";
            }
        }
        $this->renderBlock("Login");
    }

    function profile()
    {
        if ($_SESSION['user']) {
            $orderModel = new OrderModel();
            $this->data['orders'] = $orderModel->getOrdersUser($_SESSION['user']);
        }
        if(isset($_POST['pay'])){
            $html = OrderController::pay($_POST['pay']);
            $this->data['html'] = $html;
        }
        $this->data['title'] = "Личный кабинет";
        $this->render("Profile");
    }

}

?>