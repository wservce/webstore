<?php
namespace frontend\controllers;
use prototypes\Controllers;
use \system\App;
class FooterController extends Controllers{
	function __construct()
	{
		parent::__construct();
	}
	function index() {
		$this->renderBlock("Footer");
	}
}

?>