<?php
namespace frontend\controllers;

use frontend\models\HomeModel;
use frontend\models\LangModel;
use frontend\models\SeoModel;
use frontend\models\StaticModel;
use prototypes\Controllers;
use system\Helpers;

class HomeController extends Controllers
{
    function __construct()
    {
        parent::__construct();
        $this->model = new StaticModel();
    }

    function index()
    {
        $langModel = new LangModel();
        $lang = $langModel->getOneLang($_SESSION['lang']);
        $static = $this->model->getOnePageByUrl($lang['id'],"");
        $this->data['text'] = $static['text'];
        $this->data['title'] = $static['title'];
        $this->data['desc'] = $static['descriptions'];
        $this->data['key'] = $static['keywords'];
        $this->render("Home");
    }
}

?>