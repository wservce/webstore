<?php
namespace frontend\controllers;

use prototypes\Controllers;
use \system\App;
use system\Helpers;

class HeaderController extends Controllers
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->data = App::$app->data;
        $this->data['css'] = App::$app->modules->ccsCache;
        $this->renderBlock("Header");
    }

}

?>