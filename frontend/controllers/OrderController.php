<?php
namespace frontend\controllers;

use frontend\models\HomeModel;
use frontend\models\LangModel;
use frontend\models\OrderModel;
use frontend\models\SeoModel;
use frontend\models\StaticModel;
use prototypes\Controllers;
use system\App;
use system\Helpers;
use system\modules\LiqPay;

class OrderController extends Controllers
{
    function __construct()
    {
        parent::__construct();
        $this->model = new StaticModel();
    }

    function developers()
    {
        $langModel = new LangModel();
        if(isset($_POST['order'])){
            $orderModel = new OrderModel();
            $id = $orderModel->addOrder($_POST['domain1'],$_POST['domain2'],md5($_POST['domain1'].md5($_POST['domain1'])),md5($_POST['domain2'].md5($_POST['domain2'])),md5(rand(0, PHP_INT_MAX)+time()),$_SESSION['user']);
           $html = $this->pay($id);
            $this->data['html'] = $html;
        }
        $lang = $langModel->getOneLang($_SESSION['lang']);
        $this->data['page'] = $this->model->getOnePageByUrl($lang['id'], "developers");
        $this->data['title'] = $this->data['page']['title'];
        $this->data['desc'] = $this->data['page']['descriptions'];
        $this->data['key'] = $this->data['page']['keywords'];
        $this->render("Developers");
    }
    static function pay($id){
        $liqpay = new LiqPay();
        $html = $liqpay->cnb_form(array(
            'action' => 'pay',
            'amount' => 1,
            'currency' => "USD",
            'description' => "ORDER ".$id,
            'order_id' => $id,
            'version' => '3',
            'result_url' => App::$app->config['BASE_URL']."order/success",
            'sandbox' => 1,
            'language' => "ru",
            'server_url' => App::$app->config['BASE_URL'] . 'order/confirm/?id=' . $id
        ));
        return $html;
    }

    function success(){
        $this->render("Success");
    }

    function confirm(){
        $liqpay = new LiqPay();
        $data = base64_decode($_POST['data']);
        $signature = $liqpay->data_to_sign($_POST['data']);
        $data = json_decode($data,1);
        if ($signature == $_POST['signature']) {
            if ($data['status'] == "succes" || $data['status'] == "sandbox") {
                if ($data['order_id'] == $_GET['id']) {
                    $orderModel = new OrderModel();
                    $orderModel->setOrderPay($_GET['id'], 1);
                } else {
                    header("HTTP/1.0 401 API ERROR");
                }
            } else {
                header("HTTP/1.0 402 API ERROR");
            }
        } else {
            header("HTTP/1.0 403 API ERROR");
        }
    }
}

?>