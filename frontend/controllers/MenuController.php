<?php
namespace frontend\controllers;
use prototypes\Controllers;
use \system\App;

class MenuController extends Controllers{
	function __construct()
	{
		parent::__construct();
	}
	function index() {
		$cacher = App::$app->modules->serializeCache;
		$menu = $cacher->open("menu");
		if($menu)
			$this->data['menu'] = $menu[$_SESSION['lang']];
		else{
			$array = [
				"ru" => [
					"#svg_block_1" => "О нас",
					"#svg_block_2" => "Преимущества",
					"#demo-wrapper" => "Демонстрация",
					"#buy-wrapper" => "Тарифы",
					"#works" => "Клиенты",
				],
				"en" => [
					"/" => "Home",
				],
			];
			$menu = $cacher->save("menu",$array);
		}
		$this->data['url'] = $_SERVER['REQUEST_URI'];
		if(strpos($this->data['url'],"menu/") > 0)
			$this->data['url'] = "/".$_SESSION['prefix']."menu/";
		$this->renderBlock("MenuBlock");
	}
}

?>