<?php
namespace frontend\controllers;
use prototypes\Controllers;
class Error404Controller extends Controllers{
    function __construct()
    {
        parent::__construct();
    }
    function index() {
        header("HTTP/1.0 404 Not Found");
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        $this->data['title'] = "404";
        $this->data['desc'] = "desc";
        $this->data['key'] = "KEY";
        $this->render("Error404");
    }
}

?>