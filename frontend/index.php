<?php
session_start();
srand(time());
$globalConfig = require "../config/Config.php";
$thisConfig = require "config/Config.php";
$config = array_merge($globalConfig,$thisConfig);
require "../system/Class.php";
global $prefix;
$prefix = "";
try {
    \system\App::start($config);
} catch (Exception $e) {
    echo 'ERROR: ', $e->getMessage(), "\n";
}