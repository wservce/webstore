$(document).ready(function () {
    var width = $(window).width();
    var height = $(window).height();
    var svg_height = 0;
    if (width > 768) {
        svg_1(width);
        svg_2(width);
        svg_3(width);
        svg_4(width);
    } else {
        svg_1_mobile(width);
        svg_2_mobile(width);
        svg_3_mobile(width);
    }
    svg_5(width)

});

var svg_point = null;
var p3_y_svg_2 = null;
var tan;

var margin_2, y2_svg2, road;

function svg_1_mobile(width) {
    var svg_1 = Snap('#svg_block_1');
    var x1 = 0;
    var y1 = width * 0.2;
    var x2 = width;
    var y2 = 0;
    var x3 = width;
    var y3 = width;
    var x4 = 0;
    var y4 = width * 0.8;
    margin_2 = y3 - y4;
    $('#svg_block_1').height(y3);
    $('#svg_block_1').css('margin-top', '-' + y1);
    road = y1 - 90;
    $('.road-wrapper').css('top', road);
    var Poligon = svg_1.polygon(x1, y1, x2, y2, x3, y3, x4, y4);
    Poligon.attr({
        strokeWidth: 0,
        fill: '#fafafa',
    });
}

var margin_3, road2, road3;
function svg_2_mobile(width) {
    var svg = Snap('#svg_block_2');
    var x1 = 0;
    var y1 = 0;
    var x2 = width;
    var y2 = margin_2;
    var x3 = width;
    var y3 = width * 1.2;
    var x4 = 0;
    var y4 = width;
    margin_3 = y3 - y4;
    $('#svg_block_2').height(y3);
    $('#svg_block_2').css('margin-top', '-' + (margin_2 + 5));
    road2 = road + y2;
    $('.road-wrapper .img2').css('margin-top', road2 + margin_2 + 80);
    var Poligon = svg.polygon(x1, y1, x2, y2, x3, y3, x4, y4);
    Poligon.attr({
        strokeWidth: 0,
        fill: '#ffffff',
    });
}

function svg_3_mobile(width) {
    var svg = Snap('#svg_block_3');
    var x1 = 0;
    var y1 = width * 0.2;
    var x2 = width;
    var y2 = 0;
    var x3 = width;
    var y3 = width;
    var x4 = 0;
    var y4 = width * 0.8;
    $('#svg_block_3').height(y3);
    $('#svg_block_3').css('margin-top', '-' + (margin_3 + 5 + y1));
    road3 = road2 + y1;
    $('.road-wrapper .img3').css('margin-top', road3 + 70);
    var Poligon = svg.polygon(x1, y1, x2, y2, x3, y3, x4, y4);
    Poligon.attr({
        strokeWidth: 0,
        fill: '#fafafa',
    });
}

function svg_1(width) {
    var svg_1 = Snap('#svg_block_1');
    var x1 = 0;
    var x2 = width * 0.2;
    var x3 = width;
    var x4 = width;
    var x5 = width * 0.9;
    var x6 = 0;
    var y1 = width * 0.15;
    var y2 = width * 0.18;
    var y3 = 0;
    var y4 = width * 0.43;
    var y5 = width * 0.4;
    var tan = (width - x2) / y2
    var y6 = x5 / tan + y5;
    svg_point = y6 - y5;
    p3_y_svg_2 = y4 - y5;
    $('#svg_block_1').height(y6);
    $('#svg_block_1').css('margin-top', '-110px');
    var Poligon = svg_1.polygon(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5, x6, y6);
    var g = svg_1.gradient("L(0, 0, " + width + ", 800)#ff7640-#ff9540");
    Poligon.attr({
        strokeWidth: 0,
        fill: g,
    });
}
var p2_y_svg_3 = null;
var p3_y_svg_3 = null;
function svg_2(width) {
    var svg2 = Snap('#svg_block_2');
    var rightTungle = width * 0.9;
    var p5_y = width * 0.37;
    var p4_y = width * 0.35;
    var p6_y = width * 0.25;
    p2_y_svg_3 = p5_y - (width * 0.25);
    p3_y_svg_3 = p4_y - p6_y;
    $('#svg_block_2').css('height', p5_y);
    $('#svg_block_2').css('margin-top', '-' + svg_point);
    var Poligon = svg2.polygon(0, svg_point - 5, rightTungle, 0, width, p3_y_svg_2, width, p4_y, rightTungle, p5_y, 0, p6_y);
    //var f = svg2.filter(Snap.filter.shadow(10, 0, .3));
    Poligon.attr({
        strokeWidth: 0,
        fill: '#fafafa',
    });
}

var p2_y_svg_4 = null;
var p1_y_svg_4 = null;
function svg_3(width) {
    var svg3 = Snap('#svg_block_3');
    var p2_x = width * 0.9;
    var p4_y = width * 0.2;
    var p5_y = width * 0.37;
    var p5_x = width * 0.3;
    var p6_y = width * 0.25;
    p1_y_svg_4 = p6_y - p4_y;
    p2_y_svg_4 = p5_y - p4_y;
    $('#svg_block_3').css('height', p5_y + 10);
    $('#svg_block_3').css('margin-top', '-' + (svg_point - 100));
    $('#svg_block_3').css('padding-top', '10px');
    var Poligon = svg3.polygon(0, 0, p2_x, p2_y_svg_3, width, p3_y_svg_3, width, p4_y, p5_x, p5_y, 0, p6_y);
    var f = svg3.filter(Snap.filter.shadow(0, 0, 5, "#000", .3));
    Poligon.attr({
        strokeWidth: 0,
        fill: '#ffffff',
        filter: f,
    });
}

function svg_4(width) {
    var svg4 = Snap('#svg_block_4');
    var p2_x = width * 0.3;
    var p4_y = width * 0.33;
    var p5_y = width * 0.4;
    var p5_x = width * 0.85;
    var p6_y = width * 0.34;
    var p6_x = width * 0.5;
    var p7_y = width * 0.4;
    var p7_x = width * 0.15;
    var p8_y = width * 0.33;
    var p8_x = 0;
    $('#svg_block_4').css('height', p5_y + 10);
    $('#svg_block_4').css('margin-top', '-' + (svg_point - 35));
    //$('#svg_block_4').css('padding-top', '10px');
    var Poligon = svg4.polygon(0, p1_y_svg_4, p2_x, p2_y_svg_4, width, 0, width, p4_y, p5_x, p5_y, p6_x, p6_y, p7_x, p7_y, p8_x, p8_y);
    var Poligon2 = svg4.polygon(0, p1_y_svg_4, p2_x, p2_y_svg_4, width, 0, width, p4_y, p5_x, p5_y, p6_x, p6_y, p7_x, p7_y, p8_x, p8_y);
    var f = svg4.filter(Snap.filter.shadow(0, 0, 5, "#000", .3));
    Poligon.attr({
        strokeWidth: 0,
        fill: '#fafafa',
        filter: f,
    });
    Poligon2.attr({
        strokeWidth: 0,
        fill: 'url(#img_4)',
    });
}

function svg_5(width) {
    var svg5 = Snap('#svg_block_5');
    var p2_x = width * 0.5;
    var p2_y = width * 0.1;
    var p3_x = width;
    var p3_y = 0;
    var p4_x = width;
    if (width > 768) {
        var p4_y = width * 0.27;
        var p5_y = width * 0.27;
    } else {
        var p4_y = width * 0.6;
        var p5_y = width * 0.6;
    }

    var p5_x = 0;
    $('#svg_block_5').css('height', p5_y);
    var Poligon = svg5.polygon(0, 0, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y);
    var Poligon2 = svg5.polygon(0, 0, width, 0, p2_x, p2_y);
    var Poligon3 = svg5.polygon(0, 0, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y);
    var g = svg5.gradient("L(0, 0, " + p4_x + ", " + p4_y + ")#ff672b-#ff8a2b");
    var f = svg5.filter(Snap.filter.blur(3, 7));
    Poligon.attr({
        strokeWidth: 0,
        fill: 'url(#img_5)',
        filter: f,
    });
    Poligon2.attr({
        strokeWidth: 0,
        fill: '#ffffff',
    });
    Poligon3.attr({
        strokeWidth: 0,
        fill: g,
        opacity: 0.8,
    });
}
