$(document).ready(function () {

    //Таглим оверлей лефого меню.
    $('.overlay').on('click', function () {
        $(this).fadeOut();
        $('.msg-wrapper').slideUp();
    })

    $('.close').on('click', function () {
        $('.overlay').fadeOut();
        $('.msg-wrapper').slideUp();
    })

    //Красим картинки
    var img1 = $('.img1').offset().top;
    var img2 = $('.img2').offset().top;
    var img3 = $('.img3').offset().top;
    $(this).on('scroll', function () {
        var scroll = $('body').scrollTop();

        if (scroll * 1.6 > img1) {
            $('.img1').addClass('in');
        } else {
            $('.img1').removeClass('in');
        }

        if (scroll * 1.5 > img2) {
            $('.img2').addClass('in');
        } else {
            $('.img2').removeClass('in');
        }

        if (scroll * 1.3 > img3) {
            $('.img3').addClass('in');
        } else {
            $('.img3').removeClass('in');
        }

    });

    //Подсветка меню
    var item1 = $('#svg_block_1').offset().top;

    $(this).on('scroll', function () {
        var scroll = $('body').scrollTop();
        $('.menu-wrapper a').removeClass('active');
        if (scroll+item1/2 > item1){
            $('.menu-wrapper .item1').addClass('active');
        }else{
            $('.menu-wrapper .item1').removeClass('active');
        }
    });


    //Активные разделы правого меню.
    var block1 = $('#svg_block_1').offset().top;
    var block2 = $('#svg_block_2').offset().top;
    var block3 = $('.demo-wrapper').offset().top;
    var block4 = $('#buy-wrapper').offset().top;
    var block5 = $('#works').offset().top;
    $(this).on('scroll', function () {
        var scroll = $('body').scrollTop();

        if (scroll > block1 && scroll < block2){
            $('.menu-wrapper .item1').addClass('active');
            $('.menu-wrapper .p1').addClass('activep');
        }else{
            $('.menu-wrapper .item1').removeClass('active');
            $('.menu-wrapper .p1').removeClass('activep');
        }

        if (scroll > block2 && scroll < block3){
            $('.menu-wrapper .item2').addClass('active');
            $('.menu-wrapper .p2').addClass('activep');
        }else{
            $('.menu-wrapper .item2').removeClass('active');
            $('.menu-wrapper .p2').removeClass('activep');
        }

        if (scroll > block3 &&  scroll < block4){
            $('.menu-wrapper .item3').addClass('active');
            $('.menu-wrapper .p3').addClass('activep');
        }else{
            $('.menu-wrapper .item3').removeClass('active');
            $('.menu-wrapper .p3').removeClass('activep');
        }

        if (scroll > block4 && scroll < block5 ){
            $('.menu-wrapper .item4').addClass('active');
            $('.menu-wrapper .p4').addClass('activep');
        }else{
            $('.menu-wrapper .item4').removeClass('active');
            $('.menu-wrapper .p4').removeClass('activep');
        }
        if (scroll > block5){
            $('.menu-wrapper .item5').addClass('active');
            $('.menu-wrapper .p5').addClass('activep');
        }else{
            $('.menu-wrapper .item5').removeClass('active');
            $('.menu-wrapper .p5').removeClass('activep');
        }
    });

    $('.menu-wrapper li').on('click','a', function () {
        event.preventDefault();
        var id  = $(this).attr('href');
        var top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 800);
    });
});

function msgBoxShow() {
    $('.overlay').fadeIn();
    $('.msg-wrapper').slideDown();
}