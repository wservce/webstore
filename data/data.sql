-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Час створення: Гру 01 2016 р., 13:30
-- Версія сервера: 5.7.13
-- Версія PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `dron`
--

-- --------------------------------------------------------

--
-- Структура таблиці `langs`
--

CREATE TABLE IF NOT EXISTS `langs` (
  `id` int(11) NOT NULL,
  `prefix` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `langs`
--

INSERT INTO `langs` (`id`, `prefix`) VALUES
(1, 'ru'),
(2, 'en');

-- --------------------------------------------------------

--
-- Структура таблиці `static`
--

CREATE TABLE IF NOT EXISTS `static` (
  `id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `static`
--

INSERT INTO `static` (`id`, `url`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Структура таблиці `static_text`
--

CREATE TABLE IF NOT EXISTS `static_text` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text,
  `lang_id` int(11) NOT NULL,
  `static_id` int(11) NOT NULL,
  `descriptions` text,
  `keywords` text,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `static_text`
--

INSERT INTO `static_text` (`id`, `name`, `text`, `lang_id`, `static_id`, `descriptions`, `keywords`, `title`) VALUES
(1, 'DRON', '<p>DRON(Do right or not) - Новый Фреймворк</p>', 1, 1, '', '', 'DRON'),
(5, 'DRON', '<p>DRON(Do right or not) - NEW FRAMEWORK</p>', 2, 1, '', '', 'DRON');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `static_text`
--
ALTER TABLE `static_text`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang_id` (`lang_id`),
  ADD KEY `static_id` (`static_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `langs`
--
ALTER TABLE `langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблиці `static`
--
ALTER TABLE `static`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблиці `static_text`
--
ALTER TABLE `static_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `static_text`
--
ALTER TABLE `static_text`
  ADD CONSTRAINT `static_text_ibfk_1` FOREIGN KEY (`static_id`) REFERENCES `static` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `static_text_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
