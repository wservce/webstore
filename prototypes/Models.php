<?php

namespace prototypes;
use mysqli;

class Models
{
    public function toRow($res)
    {
        $row = $res->fetch_assoc();
        return $row;
    }

    public function toArray($res)
    {
        while ($row = $res->fetch_assoc()) {
            $all_row[] = $row;
        }
        return $all_row;
    }
}