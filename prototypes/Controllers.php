<?php
namespace prototypes;

use \system\App;
use system\Helpers;

class  Controllers
{
    public $model;
    public $data;
    public $valid;
    function __construct()
    {
        $this->data['baseurl'] = App::$app->config["BASE_URL"];
        $this->data['lang'] = \system\App::$app->modules->lang;
        $this->valid = \system\App::$app->modules->validation;
    }
    function render($views)
    {
        if ($this->data) {
            App::$app->data = array_merge(App::$app->data, $this->data);
        }
        extract(App::$app->data, EXTR_OVERWRITE);
        require_once App::$app->APP_PATH . "" . App::$app->config['FOLDER'] . "/views/{$views}.php";
    }
    function renderBlock($views)
    {
        if ($this->data) {
            App::$app->data = array_merge(App::$app->data, $this->data);
        }
        extract(App::$app->data, EXTR_OVERWRITE);
        require_once App::$app->APP_PATH . "" . App::$app->config['FOLDER'] . "/views/{$views}.php";
    }
    function getData()
    {
        return $this->data;
    }
}