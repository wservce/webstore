<?php
namespace system;
class Site
{
    public $config = [];
    public $data = array();
    public $modules = null;
    public $URL;
    public $PATH = null, $APP_PATH = null;
    public $route;

    function __construct($a = array())
    {
        $this->config = $a;
        $this->APP_PATH = dirname(dirname(__FILE__));
        $this->modules = new Modules($a);
        $this->uri();
    }

    function config($name = "")
    {
        if ($name == "")
            return $this->config;
        else
            return $this->config[$name];
    }

    function run()
    {
        $this->modules->init();
        if (!is_array($this->PATH)) {
            $this->URL = "/";
        }
        $mod = require_once $this->APP_PATH . "" . $this->config['FOLDER'] . "/router/Routes.php";
        $this->route = $mod['rules'];
        $uri = $this->getUri($mod['rules'], $this->URL);
        $className = str_replace("/", "\\", $this->config['FOLDER']) . "\\controllers\\" . $uri[0];
        $actionName = $uri[1];
        if (!$actionName) {
            header("Location:/404");
        } else {
            $class = new $className();
            $class->$actionName();
        }
        return true;
    }

    function getUri($rules, $url)
    {
        $contr = array_search($url, array_flip($rules));
        if ($contr) {
            $pos = strripos($contr, "/");
            return [substr($contr, 0, $pos), substr($contr, $pos + 1)];
        } else {
            $uri = explode("/", $url);
            array_shift($uri);
            array_pop($uri);
            $stack = [];
            $rul = "";
            $tr = 0;
            foreach ($rules as $i => &$r) {
                $stack = [];
                $rul = "";
                $tr = 0;
                $ruri = explode("/", $i);
                if (count($ruri) > 2) {
                    array_shift($ruri);
                }
                array_pop($ruri);
                if (count($ruri) == count($uri)) {
                    foreach ($ruri as $p => $rule) {
                        if ($rule == $uri[$p]) {
                            $tr++;
                        } else {
                            /*Регулярка  на поиск тега*/
                            $pos = preg_match('/<-\S*>/', $rule);
                            if ($pos > 0) {
                                $stack[$rule] = $uri[$p];
                                $tr++;
                            }
                        }
                    }
                }
                if (count($stack) > 0 && $tr == count($uri)) {
                    $rul = $r;
                    break;
                }
            }
            if (count($rul) > 0 && $tr == count($uri)) {
                $rul = str_replace(array_flip($stack), $stack, $rul);
                $parts = explode("/", $rul);
                $action = array_pop($parts);
                return [implode("/", $parts), $action];
            }
        }
    }

    function unurlSet($url, $prefix)
    {
        $this->UN_URL = str_replace(array("/" . $prefix, "/ru/"), "/", $url);
    }

    function uri()
    {
        $_SERVER['REQUEST_URI'] = str_replace(App::$app->config['FOLDER'], '', $_SERVER['REQUEST_URI']);
        $prefix = "";
        $dir = $this->APP_PATH . "" . $this->config['FOLDER'] . '/lang';
        $files = scandir($dir);
        $lang = array();
        $i = 0;
        foreach ($files as $l) {
            if (strlen($l) > 3) {
                $l = str_replace(".php", "", $l);
                $lang[$i] = $l;
                $i++;
            }
        }
        if ($_SERVER['REQUEST_URI'] != '/') {
            try {
                $url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
                $this->URL = $url_path;
                $uri_parts = explode('/', trim($url_path, ' /'));
                $this->unurlSet($url_path, $prefix);
                $n = count($uri_parts);
                if (in_array($uri_parts[0], $lang)) {
                    if ($_SESSION['lang'] <> $uri_parts[0]) {
                        $_SESSION['lang'] = $uri_parts[0];
                        $_SESSION['prefix'] = $uri_parts[0] . "/";
                        if ($_SESSION['lang'] == $this->config("LANG")) {
                            $url = str_replace($_SESSION['prefix'], '', $_SERVER['REQUEST_URI']);
                            $_SESSION['prefix'] = "";
                            echo "<script>location.href = \"$url\";</script>";
                        } else {
                            echo "<script>location.reload();</script>";
                        }
                    }
                    for ($i = 0; $i < $n - 1; $i++) {
                        $uri_parts[$i] = $uri_parts[$i + 1];
                    }
                    unset($uri_parts[$n - 1]);
                    $n--;
                }
                $i = 0;
                while ($i < $n) {
                    if (isset($uri_parts[$i + 1]) && count($uri_parts[$i + 1]) > 0) {
                        $this->PATH[$uri_parts[$i]] = urldecode($uri_parts[$i + 1]);
                    } else {
                        $this->PATH[$uri_parts[$i]] = '';
                    }
                    $i = $i + 2;
                }
            } catch (Exception $e) {
            }
        }
    }
}

?>