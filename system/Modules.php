<?php

namespace system;

class Modules
{
    public $db;
    public $lang;
    public $page;
    public $serializeCache;
    public $paginator;
    public $validation;
    public  $resizeImage;
    public  $ccsCache;
    function __construct($a)
    {
        $this->db = new \system\modules\Db($a);
    }
    function init(){
        $this->serializeCache = new \system\modules\SerializeCache();
        $this->resizeImage = new \system\modules\ResizeImage();
        $this->ccsCache = new \system\modules\CssCache();
        $this->lang = new \system\modules\Lang();
        $this->paginator = new \system\modules\Paginator();
        $this->validation = new \system\modules\Validation();
        $this->page = new \system\modules\Page();
    }

}