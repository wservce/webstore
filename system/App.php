<?php

namespace system;

class App {
    public static $app  = null;
    private function __construct() {
    }
    protected function __clone() {
    }
    static public function start($config) {
        if(is_null(self::$app))
        {
            self::$app = new Site($config);
            self::$app->run();
        }
        return self::$app;

    }
}