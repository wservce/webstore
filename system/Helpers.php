<?php
namespace system;
use prototypes\Controllers;
use \system\App;

class Helpers
{
    static function transliterate($string, $st)
    {
        $converter = array(
            'Ъ' => '\Tv', 'Ь' => '\Mg', 'ь' => 'j', 'ъ' => '-j',
            'ю' => 'yu', 'ш' => 'sh', 'щ' => 'sch', 'ы' => 'yi', 'е' => 'ye', 'ё' => 'yo',
            'ч' => 'ch', 'ж' => 'zh', 'я' => 'ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd',
            'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'э' => 'e',
            ' ' => '_',
            'Ю' => 'Yu',
            'Щ' => 'Sch', 'Ш' => 'Sh', 'Ч' => 'Ch',
            'Я' => 'Ya', 'Ы' => 'Yi', 'Е' => 'YE', 'Ё' => 'YO',
            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D',
            'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Э' => 'E'
        );

        if ($st == 0)
            return strtr($string, $converter);
        else
            return strtr($string, array_flip($converter));
    }

    static function pre($ar)
    {
        echo "<pre>";
        print_r($ar);
        echo "</pre>";
    }

    static function Block($controller,$action,$data = false)
    {
        $controller = str_replace("/","\\",App::$app->config['FOLDER']) . "\\controllers\\" . $controller;
        $controller = new $controller;
        if($data)
            $controller->$action($data);
        else
            $controller->$action();
    }
    static function ImgLoad($inp,$path){
        $img = '';
        $uploaddir = str_replace('\\','/', App::$app->modules->url->APP_PATH . $path);
        if(!is_dir($uploaddir)) mkdir($uploaddir) ;
        $file = substr($inp['name'], strrpos($inp['name'], '.'), 999);
        $uploadfile = $uploaddir . time().rand(1, 10) . $file;
        if (move_uploaded_file($inp['tmp_name'], $uploadfile)) {
            //  $inf = "Файл корректен и был успешно загружен.\n";
            $img = $uploadfile;
        } else {
            exit("Ошибка при загрузке картинки");
        }
        $img = str_replace(array('frontend','//'),"",(str_replace(str_replace('\\', '/', App::$app->modules->url->APP_PATH),"",$img)));
        return $img;
    }
}