<?php
namespace system\modules;

use \prototypes\Components;
use mysqli;
use system\Helpers;

class Db extends Components
{
    private $connection;
    function __construct($config)
    {
        parent::__construct($config);
        $this->open_connection(); // сразу подключает к бд
    }

    private function open_connection()
    {
        $this->connection = new mysqli($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME);
        if (!$this->connection) {
            die("Ошибка в подключении к БД:" . mysql_error());
        }
        $this->connection->query("set names utf8");
    }

    function refValues($arr)
    { // без этого метода работает не на всех версиях!
        if (strnatcmp(phpversion(), '5.3') >= 0) { //Если версия PHP >=5.3 (в младших версиях все проще)
            $refs = array();
            foreach ($arr as $key => $value) {
                $refs[$key] = &$arr[$key]; //Массиву $refs присваиваются ссылки на значения массива $arr
            }
            return $refs; //Массиву $arr присваиваются значения массива $refs
        }
        return $arr; //Возвращается массив $arr
    }

    public function sql($query, $array)
    {
        if (!($stmt = $this->connection->prepare($query))) {
            $trace = debug_backtrace();
            trigger_error("<br/><b>ON FILE:</b><br/>".$trace[0]['file']."<br/> <b>LINE</b> ".$trace[0]['line']."<br/><b>DATA:</b><br/> ".json_encode($array).'<br/>Mysqli Ошибка: <b>' . $this->connection->error . '(' . $this->connection->errno . ')</b>!'." <br/><b>QUERY:</b>".$query, E_USER_ERROR);
        }
        if (is_array($array))
            call_user_func_array(array($stmt, 'bind_param'), $this->refValues($array));
        if (!$stmt->execute()) {
            trigger_error('Not run execute: <b>' . $stmt->error . '(' . $stmt->errno . ')</b>!' . " <br/><b>QUERY:</b>" . $query, E_USER_ERROR);
        }
        $result = $stmt->get_result();

        $stmt->close();

        return $result;
    }

    public function last_id()
    {
        return $this->connection->insert_id;
    }
    public function field($table){
        $result = $this->sql("SELECT * FROM {$table} LIMIT 0,1",0);
        $array = [];
        while ($property = mysqli_fetch_field($result)) {
            $array[] = (array)$property;
        }
        return $array;

    }
}


?>