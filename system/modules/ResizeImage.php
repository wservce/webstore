<?php

namespace system\modules;

use system\App;
use system\Helpers;

class ResizeImage
{
    public function resize($filename, $hw, $type = 0)
    {
        $url = dirname($filename);
        $file = basename($filename);
        $filename = str_replace("\\", "/", App::$app->APP_PATH . App::$app->config['FOLDER'] . $filename);
        $dir = dirname($filename);
        $ret = explode(".", $file);
        list($width, $height) = getimagesize($filename);
        if ($type == 1) {
            $new_width = $hw;
            $new_height = ceil($hw * $height / $width);
        } elseif ($type == 2) {
            $new_height = $hw;
            $new_width = ceil($hw * $width / $height);
        } else {
            $new_width = $hw[0];
            $new_height = $hw[1];
        }
        if(!is_dir($dir."/thumb/")) mkdir($dir."/thumb/");
        if (!file_exists($dir."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1])) {
            if (strtolower($ret[1]) == "jpg") {
                $image_p = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromjpeg($filename);
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagejpeg($image_p, $dir."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1],50); //50% это качество 0-100%
            } elseif (strtolower($ret[1]) == "png") {
                $image_p = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefrompng($filename);
                //Отключаем режим сопряжения цветов
                imagealphablending($image_p, false);
                //Включаем афльфа
                imagesavealpha($image_p, true);
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagepng($image_p, $dir."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1],9);
            } elseif (strtolower($ret[1]) == "gif") {
                $image_p = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromgif($filename);
                //Отключаем режим сопряжения цветов
                imagealphablending($image_p, false);
                //Включаем афльфа
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagegif($image_p, $dir."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1]);
            } elseif (strtolower($ret[1]) == "bmp") {
                $image_p = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromwbmp($filename);
                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                imagewbmp($image_p, $dir."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1],50);
            }
        }
        return $url ."/thumb/".$ret[0] . "_" . $new_width . "x" . $new_height . "." . $ret[1];
    }


}