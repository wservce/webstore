<?php
namespace system\modules;

use \system\App;

class Validation
{
    function __construct()
    {

    }

    function email($data)
    {
        if (filter_var($data, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }

    function password($data)
    {
        if (!empty($data)) {
            if (strlen($data) <= '3') {
                //echo "Your Password Must Contain At Least 6 Characters!";
                } elseif (!preg_match("#[0-9]+#", $data)) {
                    //echo "Your Password Must Contain At Least 1 Number!";
                    return false;
                } elseif (!preg_match("#[A-Z]+#", $data)) {
                    //echo "Your Password Must Contain At Least 1 Capital Letter!";
                    return false;
                } elseif (!preg_match("#[a-z]+#", $data)) {
                    //echo "Your Password Must Contain At Least 1 Lowercase Letter!";
                    return false;
            } else {
                return true;
            }
        } elseif (!empty($_POST["password"])) {
            return false;
        }
    }

    function phone($data)
    {
        $codes = ["38039", "38050", "38063", "38066", "38067", "38068", "38091", "38092", "38093", "38094", "38095", "38096", "38097", "38098", "38099"];
        if (!in_array(substr($data, 0,5), $codes))
            return false;
        if (!preg_match("/^[0-9]{10,12}+$/", $data))
            return false;
        else
            return true;
    }

    function lenMore($data, $len)
    {
        if (mb_strlen($data) >= $len)
            return true;
        else
            return false;
    }
}

?>