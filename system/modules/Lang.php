<?php
namespace system\modules;
use \system\App;

class Lang{
    var $LANG = array();
    function __construct(){
        if(App::$app->config['LANG'] == "<getCache>"){
            $cacher = App::$app->modules->serializeCache;
            $set = $cacher->open("settings");
            App::$app->config['LANG'] = $set['mainLang'];
        }
        if (!isset($_SESSION['lang'])) {
            $_SESSION['lang'] = App::$app->config['LANG'];
        }
        if ($_SESSION['lang'] <> App::$app->config['LANG']) {
            $_SESSION['prefix'] = $_SESSION['lang'] . "/";
        }
        require_once App::$app->APP_PATH."".App::$app->config['FOLDER']."/lang/{$_SESSION['lang']}.php";
        $this->LANG = $lang;
    }
    function getString($key){
        $a = $this->LANG[$key];
        if($a)
            return $this->LANG[$key];
        else
            return "lang_error($key)";
    }
}
?>