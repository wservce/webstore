<?php

namespace system\modules;
use system\App;

class SerializeCache
{

    public function save($file, $data)
    {
        $serialize = serialize($data);
        fwrite(fopen( App::$app->APP_PATH."/cache/serialize/".$file, "w+"), $serialize);
    }

    public function open($file)
    {
        if(file_exists( App::$app->APP_PATH."/cache/serialize/".$file)) {
            $data = unserialize(@file_get_contents( App::$app->APP_PATH . "/cache/serialize/" . $file));
            return $data;
        }else{
            throw new \Exception('File '.App::$app->APP_PATH."/cache/serialize/".$file.' NOT Fount');
            return false;
        }
    }

}