<?php

namespace system\modules;

use system\App;

class Paginator
{
    function paginate($pages, $url)
    {
        if($pages < 1)
            $pages = 1;
        if($_GET['page'] <= $pages) {
            ?>
            <ul class="pagination">
                <?php
                $n = App::$app->config['PAGINATION_COUNT'];
                if ($pages > 1) {
                    if ($_GET['page'] > $n) {
                        ?>
                        <li><a href="<?= $this->link($url, 0) ?>">Начало</a></li>
                    <?php }
                    if ($pages - ceil($n / 2) < $_GET['page'] && $pages > $n)
                        $start = $pages - $n;
                    else
                        $start = $_GET['page'] - ceil($n / 2);
                    if ($start < 1) {
                        $start = 1;
                    }
                    for ($i = $start; $i < $start + 5 && $i <= $pages; $i++) {
                        ?>
                        <li <?php if ($_GET['page'] == $i) echo "class='active'"; ?>><a
                                href="<?= $this->link($url, $i) ?>"><?= $i ?></a></li>
                        <?
                    }
                    if ($_GET['page'] < $pages - ceil($n / 2) && $pages > $n) {
                        ?>
                        <li><a href="<?= $this->link($url, $pages) ?>">Конец</a></li>
                    <?php }
                    ?>
                <?php } ?>
            </ul>
            <?php
        }else{
            echo "<script>location.href = '/404/';</script>";
        }
    }
    function link($link,$i){
        return str_replace("<page>",$i,$link);
    }

}