<?php

namespace system\modules;

use system\App;

class CssCache
{
    var $files = array();

    public function includeCss($file)
    {
        $this->files[] = $file;
    }

    public function writeCss($name)
    {
        $folder = App::$app->config['FOLDER'];
        $url = App::$app->APP_PATH;
        if (file_exists($url . $folder . $name))
            $ft = filemtime($url . $folder . $name);
        else
            $ft = 0;
        $tr = false;
        foreach ($this->files as $file) {
            if (file_exists($url . $folder . $file)) {
                $time = filemtime($url . $folder . $file);
                if ($time <> $ft || $tr) {
                    $tr = true;
                }
            }
        }
        if ($tr) {
            $fp = fopen($url . $folder . $name, "w");
            $time = time();
            $text = "/*$time*/";
            foreach ($this->files as $file) {
                $text .= file_get_contents($url . $folder . $file, true);
                touch($url . $folder . $file, $time);
            }
            $text = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $text);
            $text = str_replace(array("\r\n", "\r", "\n", "\t"), '', $text);
            $text = str_replace(array('  ', '    ', '    '), ' ', $text);
            fwrite($fp, $text);
            fclose($fp);
            touch($url . $folder . $name, $time);
        }
        echo "<link rel=\"stylesheet\" href=\"{$name}\">";
    }

}