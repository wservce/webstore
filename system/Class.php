<?php
function classes ($class) {
    try {
        if(! @include_once dirname(dirname(__FILE__)) . "/" . str_replace("\\", "/", $class) . ".php"){
            throw new Exception("Файл не найден!");
        }
    } catch (Exception $e){
        die("Ошибка подключения класса {$class} ".$e->getMessage());
    }
};
spl_autoload_register('classes');
?>